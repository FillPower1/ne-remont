module.exports = {
    root: true,
    plugins: [
        'import',
        'eslint-comments',
        'react',
        'react-hooks'
    ],
    // Парсер
    parser: '@babel/eslint-parser',
    parserOptions: {
        // Версия экмаскрипта которую мы умеем парсить
        ecmaVersion: 2019,
        // Можно ли использовать imports
        sourceType: 'module',
        requireConfigFile: false,
        ecmaFeatures: {
            // Нужно ли парсить jsx
            jsx: true
        },
        babelOptions: {
            presets: ['@babel/preset-react']
        },
    },
    // Применяем правила с плагинов
    extends: [
        'eslint:recommended',
        'plugin:react/recommended'
    ],
    // окружение в котором работает приложение
    env: {
        browser: true,
        node: true
    },
    settings: {
        react: {
            // Указывает eslint-plugin-react автоматически определять версию React для использования
            version: 'detect'
        }
    },
    // дополнительные правила
    rules: {
        // Code-style
        camelcase: 0,
        'class-methods-use-this': 0,
        'consistent-return': 0,
        'comma-dangle': ['warn', 'only-multiline'],
        quotes: ['warn', 'single'],
        'jsx-quotes': ['warn', 'prefer-double'],
        'import/no-extraneous-dependencies': 0,
        'import/order': [
            'warn',
            {
                groups: [
                    'builtin',
                    'external',
                    'internal',
                    'parent',
                    'sibling',
                    'index'
                ],
                'newlines-between': 'always'
            }
        ],
        'import/no-duplicates': 1,
        indent: ['warn', 4],
        'max-params': 0,
        semi: 0,
        'space-in-parens': ['error', 'never'],
        'array-bracket-spacing': ['error', 'never'],
        'space-before-blocks': ['error', 'always'],
        'keyword-spacing': ['error', { before: true, after: true }],
        'eol-last': ['warn', 'always'],
        'no-cond-assign': ['error', 'except-parens'],
        'no-implicit-coercion': 0,
        'no-else-return': 0,
        'no-extra-boolean-cast': 0,
        'no-nested-ternary': 2,
        'no-plusplus': 1,
        'no-multi-spaces': 2,
        'no-constant-condition': 0,
        'no-prototype-builtins': 0,
        'no-return-await': 0,
        'no-underscore-dangle': 0,
        'no-unused-vars': 1,
        'no-use-before-define': 0,
        'no-useless-escape': 0,
        'prefer-rest-params': 0,
        'object-curly-spacing': ['warn', 'always'],
        'space-before-function-paren': [
            'error',
            {
                anonymous: 'always',
                named: 'never',
                asyncArrow: 'always'
            }
        ],
        'no-console': 'warn',
        'no-process-exit': 'error',
        'no-magic-numbers': [
            'warn',
            {
                ignore: [0, 1, -1, 100],
                ignoreArrayIndexes: false,
                enforceConst: true,
                detectObjects: false
            }
        ],

        // React
        'react/jsx-wrap-multilines': 'error',
        'react/jsx-no-bind': 0,
        'react/jsx-uses-react': 'error',
        'react/jsx-uses-vars': 'error',
        'react/display-name': 0,
        'react/prop-types': 1,
        'react/require-default-props': 1,
        'react/no-children-prop': 0,
        'react/react-in-jsx-scope': 0,
        'react/jsx-curly-spacing': [
            'error',
            {
                when: 'never'
            }
        ],
        'react/jsx-tag-spacing': [
            'warn',
            {
                closingSlash: 'never',
                beforeSelfClosing: 'always',
                afterOpening: 'never'
            }
        ],

        // React Hooks
        'react-hooks/rules-of-hooks': 2,
        'react-hooks/exhaustive-deps': 1
    }
}
