import Button from 'rambler-ui/Button'

import { Header } from '../components/header'
import ru from '../locales/ru.json'
import style from '../styles/main.module.css'

export default function Home() {
    return (
        <>
            <div className="container">
                <Header />
            </div>
            <section className={style.section}>
                <div className="container">
                    <h1 className={style.h1}>{ru['remont.title']}</h1>
                    <p className={style.description}>{ru['remont.descripiton']}</p>
                    <form className={style.search}>
                        <input className={style.searchInput} type="text" placeholder="Услуги или специалист" />
                        <Button type="primary">{ru.find}</Button>
                    </form>
                    <ul className={style.suggests}>
                        <li className={style.suggestsItem}>Ремонт Sumsung Galaxy</li>
                        <li className={style.suggestsItem}>Ремонт Indesit</li>
                        <li className={style.suggestsItem}>Ремонт Sumsung Galaxy</li>
                    </ul>
                </div>
            </section>
        </>
    )
}

export async function getStaticProps(context) {
    return {
        props: {}
    }
}
