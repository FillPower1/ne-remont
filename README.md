## Быстрый старт

1. Обязательно создать файл в проекте `.env.local` и завести переменную окружения
`NEXT_PUBLIC_API_HOST=http://localhost:8080/api`


 2. Запустить команду:

```bash
npm run start
# или
yarn dev
```

Клиент доступен по [http://localhost:3000](http://localhost:3000)  
Сервер для заглушек доступен по [http://localhost:8080/api](http://localhost:8080/api)

## Сборка для production
```bash
1. В файле .env.local задать нужный хост NEXT_PUBLIC_API_HOST=/URL
2. npm run build
```
