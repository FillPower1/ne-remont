import Link from 'next/link'
import Image from 'next/image'
import Button from 'rambler-ui/Button'

import ru from '../../locales/ru.json'
import list from '../../public/icons/ic-list.svg'
import search from '../../public/icons/ic-search.svg'
import settings from '../../public/icons/ic-setting.svg'
import actions from '../../public/icons/ic-actions.svg'

import style from './style.module.css'

export const Header = () => {
    return (
        <header className={style.header}>
            <div className={style.wrapper}>
                <nav className={style.nav}>
                    <div className={style.brand}>{ru['brand.label']}</div>
                    <ul className={style.list}>
                        <li className={style.item}>
                            <Link href="/create-order"><a className={style.link} >{ru['create.order']}</a></Link>
                        </li>
                        <li className={style.item}>
                            <Link href="/find-specialist"><a className={style.link}>{ru['find.specialist']}</a></Link>
                        </li>
                        <li className={style.item}>
                            <Link href="/become-performer"><a className={style.link}>{ru['become.performer']}</a></Link>
                        </li>
                        <li className={style.item}>
                            <Link href="/my-orders"><a className={style.link}>{ru['my.orders']}</a></Link>
                        </li>
                    </ul>
                </nav>
                <div className={style.buttons}>
                    <Button type="primary">{ru.login}</Button>
                </div>
            </div>
            <div className={style.mobileMenu}>
                <ul className={style.mobileList}>
                    <li className={style.mobileItem}>
                        <Link href="/create-order">
                            <a className={style.mobileLink} >
                                <Image src={list} width={24} height={24} />
                                <span>{ru['orders']}</span>
                            </a>
                        </Link>
                    </li>
                    <li className={style.mobileItem}>
                        <Link href="/search">
                            <a className={style.mobileLink}>
                                <Image src={search} width={24} height={24} />
                                <span>{ru['find']}</span>
                            </a>
                        </Link>
                    </li>
                    <li className={style.mobileItem}>
                        <Link href="/support">
                            <a className={style.mobileLink}>
                                <Image src={settings} width={24} height={24} />
                                <span>{ru['support']}</span>
                            </a>
                        </Link>
                    </li>
                    <li className={style.mobileItem}>
                        <Link href="/more">
                            <a className={style.mobileLink}>
                                <Image src={actions} width={24} height={24} />
                                <span>{ru['more']}</span>
                            </a>
                        </Link>
                    </li>
                </ul>
            </div>
        </header>
    )
}
