const express = require('express')
const router = require('./router')

const app = express()
const port = process.env.PORT || 8080

app
    .use((req, res, next) => {
        res.header('Access-Control-Allow-Origin', '*')
        res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
        next()
    })
    .use('/api', router)
    .listen(port, () => {
        console.log(`> stub server started on http://localhost:${port}/api`)
    })
